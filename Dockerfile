FROM python:3.10-alpine

WORKDIR /usr/src/app

RUN pip install quart
COPY . .
VOLUME ./data:/usr/src/app/data

CMD [ "python", "./main.py" ]